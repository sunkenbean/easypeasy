# EasyPeasy

Git repository for EasyPeasy, a hackbook adapting *Allen Carr's EasyWay to Stop Smoking* for porn addiction. [The website automatically compiles the LaTeX document.](https://pmohackbook.org) It uses subfiles, located in the tex directory to compile the document.

Using XeLaTeX, provided you have texlive installed, run:

```
latexmk -pdf easypeasy.tex
```

On Windows, download a tex editor such as [texworks](https://www.tug.org/texworks/) and compile it using XeLaTeX.

Versions are numbered using [semantic versioning](https://semver.org).

## Translations

Incredibly grateful and genuinely honored that people would like to translate this, however please hold off until version two is released! Currently making some changes to the book in order to make it more clear and concise and wouldn't want you to waste your time translating an older version.

Follow the gitlab releases or the [subreddit](https://reddit.com/r/pmohackbook) for announcements upon release.